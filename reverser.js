const fs = require('fs').promises;

async function main(){
    let input = await fs.open('./input.txt');
    let buffer = Buffer.from('');
    let byteRead = 1;
    let bufferPosition = 0;
    while(byteRead){
        let {
            bytesRead: w,
            buffer: b
        } = await  input.read({
            length: 1000,
            position: bufferPosition
        });

        byteRead = w;
        buffer = Buffer.concat([buffer, b], buffer.length + b.length);
        console.log(w);
        bufferPosition += byteRead;
        if(byteRead < 1000)
            break;
    }

    let text = buffer.toString();
    let lines = text.split('\n');
    let reversedText = '';
    for (const line of lines) {
        let charsOfText = Array.from(line);
        charsOfText.reverse();
        reversedText += '\n' + charsOfText.join('');
    }
    console.log('Output: ' + reversedText);

    let output = await fs.open('./output.txt', 'w');
    await output.appendFile(reversedText);
}


main();